### 概述
> chromeDriver在linux平台有更好的稳定性以及众多特性。但chromeDriver在网页内部的元素进行操作时会有特征， 
  通过修改源码的方式来改变特征。

### 特征
* chromeDriver会创建cdc_asdjflasutopfhvcZLmcfl_元素用于各种操作。比如，在使用findElement方法时，会使用该元


### 修改
* 修改js/call_function.js,129行。
    ```
    function getPageCache(opt_doc) {
      var doc = opt_doc || document;
      var key = '$cdc_asdjflasutopfhvcZLmcfl_';
      if (!(key in doc))
        doc[key] = new Cache();
      return doc[key];
    }
    ```
    -->修改后
    ```
    function getPageCache(opt_doc) {
      var doc = opt_doc || document;
      var key = '$bobo_zhangyx_';
      if (!(key in doc))
        doc[key] = new Cache();
      return doc[key];
    }
     ```
### 文件说明
- chromedriver是在Ubuntu16.04下测试通过的。
- chromedriver.exe是在windows10下测试通过的。