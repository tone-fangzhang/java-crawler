package com.zhangyx.core;

import com.alibaba.fastjson.JSONObject;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicNameValuePair;

import java.io.Serializable;
import java.util.*;

public class Page implements Serializable {
    //请求信息
    private String url;
    private Boolean useGET = true;
    private Map<String, String> header;
    private Map<String, String> requestCookies;
    private Object entity;//map<String,String> or JsonObject

    //返回信息
    private int responseCode;
    private String html;
    private Map<String, String> responseCookies;

    //参数传递
    private Map<String, String> temp = new HashMap<String, String>();


    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Boolean getUseGET() {
        return useGET;
    }

    public void setUseGET(Boolean useGET) {
        this.useGET = useGET;
    }

    public Map<String, String> getHeader() {
        return header;
    }

    public void setHeader(Map<String, String> header) {
        this.header = header;
    }

    public Map<String, String> getRequestCookies() {
        return requestCookies;
    }

    public void setRequestCookies(Map<String, String> requestCookies) {
        this.requestCookies = requestCookies;
    }

    public Object getEntity() {
        return entity;
    }

    public void setEntity(Object entity) {
        this.entity = entity;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }

    public Map<String, String> getResponseCookies() {
        return responseCookies;
    }

    public void setResponseCookies(Map<String, String> responseCookies) {
        this.responseCookies = responseCookies;
    }

    public Map<String, String> getTemp() {
        return temp;
    }

    public void setTemp(Map<String, String> temp) {
        this.temp = temp;
    }
}
