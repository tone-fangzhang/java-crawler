package com.zhangyx.comm;

import org.apache.log4j.Logger;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public class RedisUtil {
    private static JedisPool jedisPool;//非切片连接池
    private static Logger log = Logger.getLogger(RedisUtil.class);

    public static void init(String host, int port) {
        JedisPoolConfig config = new JedisPoolConfig();
        config.setMaxIdle(20);
        config.setMaxWaitMillis(10000);
        config.setTestOnBorrow(false);
        jedisPool = new JedisPool(config,host,port);
        if (!getJedis().isConnected()) {
            log.info("redis 启动失败，程序停止");
            System.exit(1);
        }else {
            log.info("redis 配置成功");
        }
    }

    public static Jedis getJedis() {
        return jedisPool.getResource();
    }

    /**
     * 多少秒过期
     * @param key
     * @param value
     * @param seconds
     */
    public static void setEx(String key, String value, int seconds) {
        Jedis jedis = null;
        try{
            jedis = getJedis();
            jedis.setex(key, seconds, value);
        }finally {
            jedis.close();
        }
    }
    public static Boolean isExist(String key) {
        Boolean result = false;
        Jedis jedis = null;
        try{
            jedis = getJedis();
            result = jedis.exists(key);
        }finally {
            jedis.close();
        }
        return result;
    }

    public static void set(String key,String value) {
        Jedis jedis = null;
        try{
            jedis = getJedis();
            jedis.set(key, value);
        }finally {
            jedis.close();
        }
    }
    public static String get(String key) {
        String result = null;
        Jedis jedis = null;
        try{
            jedis = getJedis();
            result = jedis.get(key);
        }finally {
            jedis.close();
        }
        return result;
    }
    public static String popQueue(String key) {
        String result = null;
        Jedis jedis = null;
        try{
            jedis = getJedis();
            result = jedis.lpop(key);
        }finally {
            jedis.close();
        }
        return result;
    }
    public static Long pushQueue(String key,String value) {
        Long result = null;
        Jedis jedis = null;
        try{
            jedis = getJedis();
            result = jedis.rpush(key,value);
        }finally {
            jedis.close();
        }
        return result;
    }

    public static <T>T popQueueObj(String key, Class<T> clazz) {
        byte[] result = null;
        Jedis jedis = null;
        try {
            jedis = getJedis();
            result = jedis.lpop(key.getBytes());
        } finally {
            jedis.close();
        }
        return SerializeUtils.getObjectFromBytes(clazz, result);
    }
    public static Long pushQueueObj(String key,Object value) {
        Long result = null;
        Jedis jedis = null;
        try{
            jedis = getJedis();
            result = jedis.rpush(key.getBytes(),SerializeUtils.toBytes(value));
        }finally {
            jedis.close();
        }
        return result;
    }
    public synchronized static <T> T popStackObj(String key,Class<T> clazz) {
        byte[] result = null;
        Jedis jedis = null;
        try{
            jedis = getJedis();
            result = jedis.rpop(key.getBytes());
        }finally {
            jedis.close();
        }
        return SerializeUtils.getObjectFromBytes(clazz, result);
    }
    public synchronized static Long pushStackObj(String key,Object value) {
        Long result = null;
        Jedis jedis = null;
        try{
            jedis = getJedis();
            result = jedis.rpush(key.getBytes(), SerializeUtils.toBytes(value));
        }finally {
            jedis.close();
        }
        return result;
    }

    public static Long length(String key) {
        Long result = null;
        Jedis jedis = null;
        try{
            jedis = getJedis();
            result = jedis.llen(key.getBytes());
        }finally {
            jedis.close();
        }
        return result;
    }
}
